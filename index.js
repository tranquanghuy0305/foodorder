const express = require("express"); // Tương tự : import express from "express";
const path = require("path");
const cors = require('cors');
//import thư viện mongoose
const mongoose = require('mongoose');

// Khởi tạo Express App
const app = express();

const port = 8000;

// //kết nối mongodb
// mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365")
//   .then(() => console.log("Connected to Mongo Successfully"))
//   .catch(error => handleError(error));

// //khai báo các model
// const drinkModel = require('./app/models/drink.model');
// const voucherModel = require('./app/models/voucher.model');
// const orderModel = require('./app/models/order.model');
// const userModel = require('./app/models/user.model');
//Để hiển thị ảnh cần thêm middleware static vào express

app.use(express.static(__dirname + "/app/views"));
app.use(express.json());
app.use(cors());

//Middleware in ra console thời gian hiện tại mỗi khi API gọi
app.use((request, response, next) => {
  var today = new Date();

  console.log("Current time: ", today);

  next();
});

//Middleware in ra console request method mỗi khi API gọi
app.use((request, response, next) => {
  console.log("Method: ", request.method);

  next();
});
// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/app/views/page/index.html"));
})

app.get("/cart", (request, response) => {
  console.log(__dirname);

  response.sendFile(path.join(__dirname + "/app/views/page/shopping-cart.html"));
})


app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
